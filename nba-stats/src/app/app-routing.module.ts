import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './features/auth/guards/auth.guard';
import { UserRoleEnum } from './core/enums/user-role.enum';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('./features/auth/auth.module').then((module) => module.AuthModule),
    data: {
      defaultReuseStrategy: true, // Ignore our custom route strategy
      resetReuseStrategy: true, // Logout redirect user to login and all data are destroyed
    },
  },
  {
    path: '',
    loadChildren: () =>
      import('./core/components/dashboard/dashboard.module').then(
        (module) => module.DashboardModule
      ),
    data: {
      defaultReuseStrategy: false, // Ignore our custom route strategy
      resetReuseStrategy: false, // Logout redirect user to login and all data are destroyed
    },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
