export interface User {
  id?: string;
  role?: string;
  username?: string;
  password?: string;
  subscriptionExpires?: string;
}
