export interface PlayerPosition {
  id?: string
  name?: string
  playerId?: string
  teamId?: string
  season?: string
  position?: string
  best?: boolean
  spotUp?: boolean
  driver?: boolean
  cbs?: boolean
  css?: boolean
  bhd?: boolean
  bhs?: boolean
  wtf?: boolean
}