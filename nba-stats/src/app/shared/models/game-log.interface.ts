import { Stat } from "./stat.interface";

export interface GameLog {
  averages: Stat,
  games: any
}