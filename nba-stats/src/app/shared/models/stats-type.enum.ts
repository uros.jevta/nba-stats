export enum StatsType {
  OFFENSIVE = 'offensive',
  DEFENSIVE = 'defensive'
}