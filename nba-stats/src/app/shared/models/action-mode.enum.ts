export enum ActionMode {
  ADD = 'Add',
  EDIT = 'Edit',
  VIEW = 'View',
}
