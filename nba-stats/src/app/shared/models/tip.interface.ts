export interface Tip {
  id?: string;
  text?: string;
  date?: any;
}
