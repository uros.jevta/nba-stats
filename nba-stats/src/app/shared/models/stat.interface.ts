export interface Stat {
  pts: number;
  trb: number;
  drb: number;
  orb: number;
  ast: number;
  fg: number;
  fgPct: number;
  fga: number;
  fg3: number;
  fg3Pct: number;
  fg3a: number;
  ft: number;
  ftPct: number;
  fta: number;
  mp: number;
  usg: number;
  starter: boolean;
  home: boolean;
  g: number;
  date: string;
  player: string;
  opponent: string;
  playerId: string;
}
