export interface Post {
  id?: string;
  text?: string;
  headline?: string;
  date?: any;
}
