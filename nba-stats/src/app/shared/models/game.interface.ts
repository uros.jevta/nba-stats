export interface Game {
  id?: string;
  date?: string;
  player?: string;
  position?: string;
}
