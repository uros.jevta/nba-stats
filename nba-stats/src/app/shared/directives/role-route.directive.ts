import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';

@Directive({
  standalone: true,
  selector: '[appRoleRoute]'
})
export class RoleRouteDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private authService: AuthService) { }

  @Input() set appRoleRoute(roles: string[]) {
    if(this.authService.getRoles()?.some(r => roles.includes(r))) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
    else {
      this.viewContainer.clear();
    }
    // if (!condition && !this.hasView) {
    //   this.viewContainer.createEmbeddedView(this.templateRef);
    //   this.hasView = true;
    // } else if (condition && this.hasView) {
    //   this.viewContainer.clear();
    //   this.hasView = false;
    // }
  }

}
