export function transformObjectToArray(object: any) {
  const array: { name: string; value: string; selected?: boolean }[] = [];
  Object.keys(object).forEach((key) => {
    array.push({
      value: key,
      name: object[key],
      selected: true,
    });
  });
  return array;
}

export const removeUndefinedValues = (objectIn: { [x: string]: any }) =>
  Object.keys(objectIn).reduce((prev, curr) => {
    return objectIn[curr] !== undefined
      ? {
          ...prev,
          [curr]: objectIn[curr],
        }
      : prev;
  }, {});

export function calculateAverage(arr: any) {
  const average = arr
    .reduce((a: any, c: any) => {
      for (let key in c) {
        let line = a.find((x: any) => x.k === key);
        if (!line) a.push((line = { k: key, sum: 0, nb: 0 }));
        line.sum += c[key];
        ++line.nb;
      }
      return a;
    }, [])
    .reduce((a: any, c: any) => {
      a[c.k] = c.sum / c.nb;
      return a;
    }, {});

  return average;
}

export function sortArrayObjectsByDate(array: any[]) {
  return function (a: { date: string }, b: { date: string }) {
    let aa = a.date.split('-').reverse().join();
    let bb = b.date.split('-').reverse().join();
    return aa < bb ? -1 : aa > bb ? 1 : 0;
  };
}

export function sortArrayObjectsByDateDesc(array: any[]) {
  return function (a: { date: string }, b: { date: string }) {
    let aa = a.date.split('-').reverse().join();
    let bb = b.date.split('-').reverse().join();
    return aa > bb ? -1 : aa < bb ? 1 : 0;
  };
}

export function trackById(index: number, item: any): number {
  return item.id;
}

export const objectsEqual: any = (o1: any, o2: any) =>
  typeof o1 === 'object' && Object.keys(o1).length > 0
    ? Object.keys(o1)?.length === Object.keys(o2).length &&
      Object.keys(o1)?.every((p: any) => objectsEqual(o1[p], o2[p]))
    : o1 === o2;

export const arraysEqual = (a1: any, a2: any) =>
  a1?.length === a2?.length &&
  a1?.every((o: any, idx: any) => objectsEqual(o, a2[idx]));

export function maxSeasonIndex(seasons: string[]) {
  let max = seasons[0];
  let maxIndex = 0;
  seasons.forEach((season, index) => {
    if (season.split('_') > max.split('_')) {
      max = season;
      maxIndex = index;
    }
  });
  return maxIndex;
}
