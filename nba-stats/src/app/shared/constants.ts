export const gameFilter = [
  { name: 'Overall', value: 'Overall' },
  { name: 'Home', value: 'Home' },
  { name: 'Away', value: 'Away' },
  { name: 'Win', value: 'Win' },
  { name: 'Lost', value: 'Lost' },
  { name: 'East', value: 'East' },
  { name: 'West', value: 'West' },
  { name: 'Home_East', value: 'Home_East' },
  { name: 'Home_West', value: 'Home_West' },
  { name: 'Away_East', value: 'Away_East' },
  { name: 'Away_West', value: 'Away_West' },
];

export const gameFilterLog = [
  ...gameFilter,
  { name: '0_Day_Rest', value: '0_day_rest' },
  { name: '1_Day_Rest', value: '1_day_rest' },
  { name: '2_Day_Rest', value: '2_day_rest' },
  { name: '3_Day_Rest', value: '3_day_rest' },
];

// export const specialCategory = [
//   { name: 'best', value: 'best' },
//   { name: 'spotUp', value: 'spotup' },
//   { name: 'driver', value: 'driver' },
//   { name: 'cbs', value: 'cbs' },
//   { name: 'css', value: 'css' },
//   { name: 'bhd', value: 'bhd' },
//   { name: 'wtf', value: 'wtf' },
//   { name: 'sus', value: 'sus' },
// ];

export const specialCategory = [
  { name: 'BP', value: 'Best' },
  { name: 'SU', value: 'SpotUp' },
  { name: 'Driver', value: 'Driver' },
  { name: 'CP', value: 'CBS' },
  { name: 'CS', value: 'CSS' },
  { name: 'BHD', value: 'BHD' },
  { name: 'BHS', value: 'BHS' },
  { name: 'MS', value: 'WTF' },
  { name: 'SUS', value: 'SUS' },
];
