import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { Stat } from '@models/stat.interface';

@Component({
  selector: 'app-game-details',
  standalone: true,
  imports: [CommonModule, TableModule],
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.scss']
})
export class GameDetailsComponent {
  @Input() gameDetails!: Stat[][]
}
