import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { GameDetailsComponent } from 'src/app/shared/components/game-details/game-details.component';
import { LoaderService } from 'src/app/core/services/loader.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { StatsService } from 'src/app/core/services/stats.service';
import { gameFilterLog, specialCategory } from 'src/app/shared/constants';
import {
  transformObjectToArray,
  maxSeasonIndex,
  removeUndefinedValues,
} from 'src/app/shared/utils';

@Component({
  selector: 'app-player-positions',
  standalone: true,
  imports: [
    CommonModule,
    TableModule,
    DropdownModule,
    MultiSelectModule,
    TabViewModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    GameDetailsComponent,
  ],
  templateUrl: './player-positions.component.html',
  styleUrls: ['./player-positions.component.scss'],
})
export class PlayerPositionsComponent {
  specialPosition!: any[];
  formGroup!: FormGroup;
  seasons!: string[];
  teams!: {
    name: string;
    value: string;
  }[];
  categories = specialCategory;
  loading = false;
  gameDetails!: any;

  constructor(
    protected statsService: StatsService,
    protected staticDataService: StaticDataService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initSeasonsAndTeams();
  }

  initSeasonsAndTeams() {
    this.loaderService.show();

    this.staticDataService.getSeasons().subscribe((seasons) => {
      this.seasons = seasons;

      this.initFiltersFromGroup();
      this.initFiltersFormListener();

      this.loaderService.hide();
    });
  }

  initFiltersFromGroup() {
    this.formGroup = new FormGroup({
      category: new FormControl<any | null>(null),
      season: new FormControl<any | null>(
        this.seasons[maxSeasonIndex(this.seasons)]
      ),
    });
  }

  initFiltersFormListener() {
    this.formGroup.valueChanges.subscribe((formValue) => {
      // if (!formValue?.team?.value) {
      //   return;
      // }

      let params = {
        category: formValue.category?.value ?? undefined,
        season: formValue.season ?? undefined,
      };

      const formattedParams = removeUndefinedValues(params);

      const isEveryFilterUsed = !Object.values(params).some(
        (el) => el === undefined
      );

      if (isEveryFilterUsed) {
        this.loaderService.show();
        this.statsService
          .getPlayerSpecialPositions(formattedParams)
          .subscribe((specialPositions) => {
            this.specialPosition = specialPositions;
            this.loaderService.hide();
            //this.loading = false;
          });
      }
    });
  }
}
