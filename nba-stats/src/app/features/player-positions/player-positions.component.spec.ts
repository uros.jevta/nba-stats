import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerPositionsComponent } from './player-positions.component';

describe('PlayerPositionsComponent', () => {
  let component: PlayerPositionsComponent;
  let fixture: ComponentFixture<PlayerPositionsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PlayerPositionsComponent]
    });
    fixture = TestBed.createComponent(PlayerPositionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
