import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { GameLog } from '@models/game-log.interface';
import { Stat } from '@models/stat.interface';
import { forkJoin } from 'rxjs';
import { LoaderService } from 'src/app/core/services/loader.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { StatsService } from 'src/app/core/services/stats.service';
import { gameFilter, gameFilterLog } from 'src/app/shared/constants';
import {
  maxSeasonIndex,
  removeUndefinedValues,
  transformObjectToArray,
} from 'src/app/shared/utils';

@Component({
  selector: 'app-game-log',
  templateUrl: './game-log.component.html',
  styleUrls: ['./game-log.component.scss'],
})
export class GameLogComponent implements OnInit {
  gameLog!: GameLog[];
  formGroup!: FormGroup;
  seasons!: string[];
  teams!: {
    name: string;
    value: string;
  }[];
  gameFilter = gameFilterLog;
  loading = false;
  gameDetails!: any;

  constructor(
    protected statsService: StatsService,
    protected staticDataService: StaticDataService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initSeasonsAndTeams();
  }

  initSeasonsAndTeams() {
    this.loaderService.show()

    forkJoin([
      this.staticDataService.getSeasons(),
      this.staticDataService.getTeams(),
    ]).subscribe(([seasons, teams]) => {
      this.seasons = seasons;
      this.teams = transformObjectToArray(teams);

      
      

      this.initFiltersFromGroup();
      this.initFiltersFormListener();

      this.loaderService.hide()
    });
  }

  initFiltersFromGroup() {
    this.formGroup = new FormGroup({
      gameFilter: new FormControl<any | null>(this.gameFilter[0]),
      team: new FormControl<any | null>(null),
      season: new FormControl<any | null>(
        this.seasons[maxSeasonIndex(this.seasons)]
      ),
    });
  }

  initFiltersFormListener() {
    this.formGroup.valueChanges.subscribe((formValue) => {
      

      if (!formValue?.team?.value) {
        return;
      }

      let params = {
        gameFilter: formValue.gameFilter?.value ?? undefined,
        teamId: formValue.team?.value ?? undefined,
        season: formValue.season ?? undefined,
      };
      

      const formattedParams = removeUndefinedValues(params);

      this.loaderService.show()

      this.statsService.getGameLogs(formattedParams).subscribe((gameLogs) => {
        
        this.gameLog = gameLogs.map((gameLog) => {
          return {
            ...gameLog,
            games: gameLog.games.map((value: any, index: any) => {
              return {
                ...value,
                index,
              };
            }),
          };
        });
        this.loaderService.hide()
        //this.loading = false;
      });
    });
  }

  onDateClick(event: any) {
    this.loaderService.show()
    // this.loading = true;
    
    const queryParams = {
      date: event.data.date,
      playerId: event.data.playerId,
    };
    this.statsService.getGameDetails(queryParams).subscribe((gameDetails) => {
      
      this.gameDetails = gameDetails;

      event.data = this.gameDetails;
      this.loaderService.hide()
      // this.loading = false;
    });
  }
}
