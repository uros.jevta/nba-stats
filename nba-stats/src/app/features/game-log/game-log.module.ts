import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameLogComponent } from './game-log.component';
import { GameLogRoutingModule } from './game-log-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { GameDetailsComponent } from 'src/app/shared/components/game-details/game-details.component';

@NgModule({
  declarations: [GameLogComponent],
  imports: [
    CommonModule,
    GameLogRoutingModule,
    TableModule,
    DropdownModule,
    MultiSelectModule,
    TabViewModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    GameDetailsComponent
  ],
})
export class GameLogModule {}
