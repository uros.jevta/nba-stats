import { Component } from '@angular/core';
import { ActionMode } from '@models/action-mode.enum';
import { Tip } from '@models/tip.interface';
import { format } from 'date-fns';
import { ConfirmationService, Message, MessageService } from 'primeng/api';
import { map } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { LoaderService } from 'src/app/core/services/loader.service';
import { TipsService } from 'src/app/core/services/tips.service';
import { trackById } from 'src/app/shared/utils';

@Component({
  selector: 'app-tips',
  templateUrl: './tips.component.html',
  styleUrls: ['./tips.component.scss'],
  providers: [MessageService, ConfirmationService],
})
export class TipsComponent {
  newTip!: Tip;
  tips!: Tip[];
  tipDialog = false;
  tipConfirmDialog = false;
  submitted: boolean = false;

  ActionMode = ActionMode;
  actionMode!: string;

  loading = false;
  trackById = trackById;
  isAdmin = this.authService.isAdmin();

  messageConfigAdd = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Tip Created',
    life: 3000,
  };
  messageConfigEdit = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Tip Updated',
    life: 3000,
  };
  messageConfigDelete = {
    severity: 'info',
    summary: 'Confirmed',
    detail: 'Tip deleted',
    life: 3000,
  };

  constructor(
    private tipsService: TipsService,
    // private staticDataService: StaticDataService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private authService: AuthService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initTips();
  }

  initTips(messageConfig?: Message) {
    this.loaderService.show();
    // this.loading = true;

    this.tipsService
      .getTips()
      .pipe(
        map((value) => {
          return value.map((tip) => {
            return {
              ...tip,
              date: format(new Date(tip.date), 'dd/MM/yyyy'),
            };
          });
        })
      )
      .subscribe((Tips) => {
        this.tips = Tips;
        // this.Tips = this.test;
        

        if (messageConfig) {
          this.tipDialog = false;
          this.messageService.add(messageConfig);
          this.newTip = {};
        }

        this.loaderService.hide();

        // this.loading = false;
      });
  }

  openNew() {
    this.actionMode = ActionMode.ADD;
    this.newTip = {};
    this.submitted = false;
    this.tipDialog = true;
  }

  openEdit(tip: Tip) {
    this.actionMode = ActionMode.EDIT;
    this.newTip = { ...tip };
    this.submitted = false;
    this.tipDialog = true;
  }

  hideDialog() {
    this.tipDialog = false;
    this.submitted = false;
  }

  saveTip() {
    this.submitted = true;

    if (this.newTip.text?.trim()) {
      this.newTip = {
        ...this.newTip,
        date: Date.now(),
      };

      if (this.actionMode === ActionMode.ADD) {
        this.loaderService.show()
        // this.loading = true;
        this.tipsService.addTip(this.newTip).subscribe((tip) => {
          // this.messageService.add({
          //   severity: 'success',
          //   summary: 'Successful',
          //   detail: 'Tip Created',
          //   life: 3000,
          // });

          // this.TipDialog = false;
          // this.newTip = {};

          this.initTips(this.messageConfigAdd);

          // this.loading = false;
        });
      } else {
        this.loaderService.show()
        // this.loading = true;
        this.tipsService.editTip(this.newTip).subscribe((tip) => {
          // this.messageService.add({
          //   severity: 'success',
          //   summary: 'Successful',
          //   detail: 'Tip Updated',
          //   life: 3000,
          // });

          // this.TipDialog = false;
          // this.newTip = {};

          this.initTips(this.messageConfigEdit);

          // this.loading = false;
        });
      }
    }
  }

  showTip(tip: Tip) {
    this.actionMode = ActionMode.VIEW;
    this.newTip = { ...tip };
    this.submitted = false;
    this.tipDialog = true;
  }

  deleteTip(tip: Tip) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this tip?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        // this.loading = true;
        this.loaderService.show()

        
        this.tipsService.deleteTip(tip.id).subscribe(() => {
          // this.messageService.add({
          //   severity: 'info',
          //   summary: 'Confirmed',
          //   detail: 'Tip deleted',
          // });

          this.initTips(this.messageConfigDelete);
          // this.loading = false;
        });
      },
    });
  }
}
