import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ToastModule } from 'primeng/toast';
import { EmailService } from 'src/app/core/services/email.service';
import { MessageService } from 'primeng/api';
import { LoaderService } from 'src/app/core/services/loader.service';

interface Message {
  email?: string;
  subject?: string;
  text?: string;
}

@Component({
  selector: 'app-contact',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    InputTextModule,
    DialogModule,
    ToastModule,
    InputTextareaModule,
    ConfirmDialogModule,
    CardModule,
  ],
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  providers: [MessageService],
})
export class ContactComponent {
  message!: Message;
  submitted = false;
  loading = false;

  messageSent = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Message sent',
    life: 3000,
  };

  constructor(
    private emailService: EmailService,
    private messageService: MessageService,
    private loaderService: LoaderService
  ) {
    this.message = {};
  }

  sendMessage() {
    this.submitted = true;

    if (
      this.message.email?.trim() &&
      this.message.subject?.trim() &&
      this.message.text?.trim()
    ) {
      this.loaderService.show();
      // this.loading = true;
      this.emailService.sendMessage(this.message).subscribe(() => {
        this.messageService.add(this.messageSent);
        this.loaderService.hide();
        // this.loading = false;
      });
    }
  }

  openInstagramPage() {
    window.open('https://www.instagram.com/fox_nba_tips/', '_blank');
  }
}
