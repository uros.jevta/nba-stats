import { Component, OnInit } from '@angular/core';
import { ActionMode } from '@models/action-mode.enum';
import { User } from '@models/user.interface';
import { ConfirmationService, Message, MessageService } from 'primeng/api';
import { UserService } from 'src/app/core/services/user.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { LoaderService } from 'src/app/core/services/loader.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  providers: [MessageService, ConfirmationService],
})
export class AdminComponent implements OnInit {
  newUser!: User;
  users!: User[];
  roles!: string[];
  clonedUsers: { [s: string]: User } = {};
  userDialog = false;
  submitted: boolean = false;
  actionMode!: string;

  actionModeEnum = ActionMode

  loading = false;

  messageConfigAdd = {
    severity: 'success',
    summary: 'Successful',
    detail: 'User Created',
    life: 3000,
  };
  messageConfigEdit = {
    severity: 'success',
    summary: 'Successful',
    detail: 'User Updated',
    life: 3000,
  };
  messageConfigDelete = {
    severity: 'info',
    summary: 'Confirmed',
    detail: 'User deleted',
    life: 3000,
  };

  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  //   {
  //     id: 'ff8080818af25f23018af25f7af011a8',
  //     username: 'djoka@djoka.com',
  //     role: 'ROLE_SUPER_ADMIN',
  //   },
  // ];

  constructor(
    private userService: UserService,
    private staticDataService: StaticDataService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initUsers();
    this.initRoles();
  }

  initUsers(messageConfig?: Message) {
    this.loaderService.show();
    this.userService.getUsers().subscribe((users) => {
      this.users = users;
      // this.users = this.test;
      

      if (messageConfig) {
        this.userDialog = false;
        this.messageService.add(messageConfig);
        this.newUser = {};
      }

      // this.loading = false;
      this.loaderService.hide()
    });
  }

  initRoles() {
    this.staticDataService.getRoles().subscribe((roles) => {
      this.roles = roles;
      
    });
  }

  openNew() {
    this.actionMode = ActionMode.ADD;
    this.newUser = {};
    this.submitted = false;
    this.userDialog = true;
  }

  openEdit(user: User) {
    this.actionMode = ActionMode.EDIT;
    this.newUser = { ...user };
    this.submitted = false;
    this.userDialog = true;
  }

  hideDialog() {
    this.userDialog = false;
    this.submitted = false;
  }

  saveUser() {
    this.submitted = true;

    if (this.newUser.username?.trim() && this.newUser.password?.trim()) {
      this.loaderService.show();
      if (this.actionMode === ActionMode.ADD) {
        this.userService.addUser(this.newUser).subscribe((user) => {
          this.initUsers(this.messageConfigAdd);
        });
      }
    } else {
      if (this.newUser.username?.trim()) {
        this.userService.editUser(this.newUser).subscribe((user) => {
          this.initUsers(this.messageConfigEdit);
        });

      }
    }
      
    // if (this.newUser.username?.trim() && this.newUser.password?.trim()) {
    //   if (this.actionMode === ActionMode.ADD) {
    //     this.userService.addUser(this.newUser).subscribe((user) => {
    //       this.initUsers(this.messageConfigAdd);
    //     });
    //   } else {
    //     this.userService.editUser(this.newUser).subscribe((user) => {
    //       this.initUsers(this.messageConfigEdit);
    //     });
    //   }
    // }
  }

  deleteUser(user: User) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this user?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.loaderService.show()
        // this.loading = true;
        this.userService.deleteUser(user.id).subscribe(() => {
          this.initUsers(this.messageConfigDelete);
        });
      },
    });
  }
}
