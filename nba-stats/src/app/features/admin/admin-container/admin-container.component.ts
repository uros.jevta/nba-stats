import { Component } from '@angular/core';

@Component({
  selector: 'app-admin-container',
  templateUrl: './admin-container.component.html',
  styleUrls: ['./admin-container.component.scss']
})
export class AdminContainerComponent {
  tabChanged = false;

  onTabChange() {
    this.tabChanged = true;
  }
}
