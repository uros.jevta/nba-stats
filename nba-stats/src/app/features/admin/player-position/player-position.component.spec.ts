import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerPositionComponent } from './player-position.component';

describe('PlayerPositionComponent', () => {
  let component: PlayerPositionComponent;
  let fixture: ComponentFixture<PlayerPositionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlayerPositionComponent]
    });
    fixture = TestBed.createComponent(PlayerPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
