import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActionMode } from '@models/action-mode.enum';
import { PlayerPosition } from '@models/player-position.interface';
import { MessageService, ConfirmationService, Message } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { LoaderService } from 'src/app/core/services/loader.service';
import { PlayerPositionService } from 'src/app/core/services/player-position.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { arraysEqual, transformObjectToArray } from 'src/app/shared/utils';

@Component({
  selector: 'app-player-position',
  templateUrl: './player-position.component.html',
  styleUrls: ['./player-position.component.scss'],
  providers: [MessageService, ConfirmationService],
})
export class PlayerPositionComponent {
  teams!: {
    name: string;
    value: string;
  }[];
  players!: {
    name: string;
    value: string;
  }[];

  newPlayerPosition!: any;
  playerPositions!: PlayerPosition[];
  originalPlayerPositions!: PlayerPosition[];
  playerPositionDialog = false;
  playerPositionConfirmDialog = false;
  submitted: boolean = false;

  ActionMode = ActionMode;
  actionMode!: string;

  formGroup!: FormGroup;

  loading = false;

  messageConfigAdd = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Player category Created',
    life: 3000,
  };
  messageConfigEdit = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Player categories Updated',
    life: 3000,
  };
  messageConfigDelete = {
    severity: 'info',
    summary: 'Confirmed',
    detail: 'PlayerPosition deleted',
    life: 3000,
  };

  positionOptions = ['PG', 'SG', 'SF', 'PF', 'C'];

  get isSaveChangesDisabled() {
    let ada = arraysEqual(this.originalPlayerPositions, this.playerPositions);

    return (
      arraysEqual(this.originalPlayerPositions, this.playerPositions) ||
      !this.playerPositions
    );
  }

  constructor(
    private playerPositionsService: PlayerPositionService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private staticDataService: StaticDataService,
    private loaderService: LoaderService
  ) {
    this.initFiltersFromGroup();
  }

  ngOnInit(): void {
    this.initTeamsAndPlayers();
  }

  initTeamsAndPlayers() {
    forkJoin([
      this.staticDataService.getPlayers(),
      this.staticDataService.getTeams(),
    ]).subscribe(([players, teams]) => {
      this.players = transformObjectToArray(players);
      this.teams = transformObjectToArray(teams);
    });
  }

  initPlayerPositions(messageConfig?: Message) {
    this.loaderService.show();
    // this.loading = true;

    if (this.formGroup.controls['teamId'].value?.value) {
      this.playerPositionsService
        .getPlayerPositions(this.formGroup.controls['teamId'].value.value)
        .subscribe((playerPositions) => {
          this.playerPositions = playerPositions;
          this.originalPlayerPositions = structuredClone(playerPositions);
          // this.originalPlayerPositions = JSON.parse(JSON.stringify(playerPositions));

          if (messageConfig) {
            this.playerPositionDialog = false;
            this.messageService.add(messageConfig);
            this.newPlayerPosition = {};
          }

          this.loaderService.hide();
          // this.loading = false;
        });
    } else {
      this.loaderService.hide();
      // this.loading = false;
      this.playerPositionDialog = false;
    }
  }

  initFiltersFromGroup() {
    this.formGroup = new FormGroup({
      teamId: new FormControl<any | null>(null),
    });

    this.initFiltersFormListener();
  }

  initFiltersFormListener() {
    this.formGroup.valueChanges.subscribe((formValue) => {
      let teamId = formValue.teamId.value;

      this.loaderService.show();
      // this.loading = true;
      this.playerPositionsService
        .getPlayerPositions(teamId)
        .subscribe((playerPositions) => {
          this.playerPositions = playerPositions;
          this.originalPlayerPositions = structuredClone(playerPositions);

          this.loaderService.hide()
          // this.loading = false;
        });
    });
  }

  openNew() {
    this.actionMode = ActionMode.ADD;
    this.newPlayerPosition = {};
    this.submitted = false;
    this.playerPositionDialog = true;
  }

  openEdit(playerPosition: PlayerPosition) {
    this.actionMode = ActionMode.EDIT;
    this.newPlayerPosition = { ...playerPosition };
    this.submitted = false;
    this.playerPositionDialog = true;
  }

  hideDialog() {
    this.playerPositionDialog = false;
    this.submitted = false;
  }

  savePlayerPosition() {
    this.submitted = true;

    let newPlayerPositionCopy = {
      ...this.newPlayerPosition,
      teamId: this.newPlayerPosition.teamId?.value,
      playerId:
        this.newPlayerPosition.playerId?.value ??
        this.newPlayerPosition.playerId,
      name: !this.newPlayerPosition.playerId?.value
        ? this.newPlayerPosition.playerId
        : this.newPlayerPosition.playerId?.value,
    };

    if (
      newPlayerPositionCopy.playerId?.trim() &&
      newPlayerPositionCopy.position?.trim() &&
      newPlayerPositionCopy.teamId?.trim()
    ) {
      this.loaderService.show()
      // this.loading = true;
      this.playerPositionsService
        .addPlayerPosition(newPlayerPositionCopy)
        .subscribe((playerPosition) => {
          this.initPlayerPositions(this.messageConfigAdd);
        });
    }
  }

  showPlayerPosition(playerPosition: PlayerPosition) {
    this.actionMode = ActionMode.VIEW;
    this.newPlayerPosition = { ...playerPosition };
    this.submitted = false;
    this.playerPositionDialog = true;
  }

  deletePlayerPosition(playerPosition: PlayerPosition) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this playerPosition?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
      this.loaderService.show()
        // this.loading = true;
        this.playerPositionsService
          .deletePlayerPosition(playerPosition.id)
          .subscribe(() => {
            this.initPlayerPositions(this.messageConfigDelete);
          });
      },
    });
  }

  saveChanges() {
    this.loaderService.show()
    
    // this.loading = true;
    this.playerPositionsService
      .editPlayerPosition(this.playerPositions)
      .subscribe(() => {
        this.messageService.add(this.messageConfigEdit);
        this.originalPlayerPositions = structuredClone(this.playerPositions);

        this.loaderService.hide()
        //this.loading = false;
      });
  }

  addNewTest() {
    this.playerPositions.unshift({});
  }
}
