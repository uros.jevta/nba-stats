import { Component, OnInit } from '@angular/core';
import { ActionMode } from '@models/action-mode.enum';
import { Game } from '@models/game.interface';
import { ConfirmationService, Message, MessageService } from 'primeng/api';
import { GameService } from 'src/app/core/services/game.service';
import { LoaderService } from 'src/app/core/services/loader.service';
import { sortArrayObjectsByDateDesc } from 'src/app/shared/utils';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
  providers: [MessageService, ConfirmationService],
})
export class GamesComponent implements OnInit {
  newGame!: Game;
  games!: any[];
  gameDialog = false;
  gameConfirmDialog = false;
  submitted: boolean = false;

  ActionMode = ActionMode;
  actionMode!: string;

  loading = false;

  messageConfigAdd = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Game Created',
    life: 3000,
  };
  messageConfigEdit = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Game Updated',
    life: 3000,
  };
  messageConfigDelete = {
    severity: 'info',
    summary: 'Confirmed',
    detail: 'Game deleted',
    life: 3000,
  };

  positionOptions = ['PG', 'SG', 'SF', 'PF', 'C'];

  constructor(
    private gamesService: GameService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initGames();
  }

  initGames(messageConfig?: Message) {
    this.loaderService.show();
    // this.loading = true;

    this.gamesService.getGames().subscribe((games) => {
      this.games = games;
      this.games.sort(sortArrayObjectsByDateDesc(this.games));

      if (messageConfig) {
        this.gameDialog = false;
        this.messageService.add(messageConfig);
        this.newGame = {};
      }

      this.loaderService.hide();
      // this.loading = false;
    });
  }

  openNew() {
    this.actionMode = ActionMode.ADD;
    this.newGame = {};
    this.submitted = false;
    this.gameDialog = true;
  }

  openEdit(game: Game) {
    this.actionMode = ActionMode.EDIT;
    this.newGame = { ...game };
    this.submitted = false;
    this.gameDialog = true;
  }

  hideDialog() {
    this.gameDialog = false;
    this.submitted = false;
  }

  saveGame() {
    this.submitted = true;

    if (this.newGame.player?.trim() && this.newGame.position?.trim()) {
      // this.newGame = {
      //   ...this.newGame,
      //   date: Date.now(),
      // };

      if (this.actionMode === ActionMode.ADD) {
        this.loaderService.show();
        // this.loading = true;
        this.gamesService.addGame(this.newGame).subscribe((game) => {
          this.initGames(this.messageConfigAdd);
        });
      } else {
        this.loaderService.show();
        // this.loading = true;
        this.gamesService.editGame(this.newGame).subscribe((game) => {
          this.initGames(this.messageConfigEdit);
        });
      }
    }
  }

  showGame(game: Game) {
    this.actionMode = ActionMode.VIEW;
    this.newGame = { ...game };
    this.submitted = false;
    this.gameDialog = true;
  }

  deleteGame(game: Game) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this game?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.loaderService.show();
        // this.loading = true;
        this.gamesService.deleteGame(game.id).subscribe(() => {
          this.initGames(this.messageConfigDelete);
        });
      },
    });
  }
}
