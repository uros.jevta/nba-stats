import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AdminContainerComponent } from './admin-container/admin-container.component';
import { GamesModule } from './games/games.module';
import { GamesComponent } from './games/games.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PlayerPositionComponent } from './player-position/player-position.component';
import { CheckboxModule } from 'primeng/checkbox';

@NgModule({
  declarations: [AdminComponent, AdminContainerComponent, GamesComponent, PlayerPositionComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    TableModule,
    DropdownModule,
    MultiSelectModule,
    TabViewModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    InputTextModule,
    DialogModule,
    ToastModule,
    ConfirmDialogModule,
    InputTextareaModule,
    GamesModule,
    CheckboxModule
  ],
})
export class AdminModule {}
