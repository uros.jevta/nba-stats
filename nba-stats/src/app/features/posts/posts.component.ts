import { Component, OnInit } from '@angular/core';
import { ActionMode } from '@models/action-mode.enum';
import { Post } from '@models/post.interface';
import { format } from 'date-fns';
import { ConfirmationService, Message, MessageService } from 'primeng/api';
import { map } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { LoaderService } from 'src/app/core/services/loader.service';
import { PostsService } from 'src/app/core/services/posts.service';
import { trackById } from 'src/app/shared/utils';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
  providers: [MessageService, ConfirmationService],
})
export class PostsComponent implements OnInit {
  newPost!: Post;
  posts!: Post[];
  postDialog = false;
  postConfirmDialog = false;
  submitted: boolean = false;

  ActionMode = ActionMode;
  actionMode!: string;

  loading = false;
  trackById = trackById;
  isAdmin = this.authService.isAdmin();

  messageConfigAdd = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Post Created',
    life: 3000,
  };
  messageConfigEdit = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Post Updated',
    life: 3000,
  };
  messageConfigDelete = {
    severity: 'info',
    summary: 'Confirmed',
    detail: 'Post deleted',
    life: 3000,
  };

  constructor(
    private postsService: PostsService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private authService: AuthService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initPosts();
  }

  initPosts(messageConfig?: Message) {
    this.loaderService.show();
    // this.loading = true;

    this.postsService
      .getPosts()
      .pipe(
        map((value) => {
          return value.map((post) => {
            return {
              ...post,
              date: format(new Date(post.date), 'dd/MM/yyyy'),
            };
          });
        })
      )
      .subscribe((posts) => {
        this.posts = posts;
        // this.posts = this.test
        
        

        if (messageConfig) {
          this.postDialog = false;
          this.messageService.add(messageConfig);
          this.newPost = {};
        }

        this.loaderService.hide();
        // this.loading = false;
      });
  }

  openNew() {
    this.actionMode = ActionMode.ADD;
    this.newPost = {};
    this.submitted = false;
    this.postDialog = true;
  }

  openEdit(post: Post) {
    this.actionMode = ActionMode.EDIT;
    this.newPost = { ...post };
    this.submitted = false;
    this.postDialog = true;
  }

  hideDialog() {
    this.postDialog = false;
    this.submitted = false;
  }

  savePost() {
    this.submitted = true;

    if (this.newPost.headline?.trim() && this.newPost.text?.trim()) {
      this.newPost = {
        ...this.newPost,
        date: Date.now(),
      };

      if (this.actionMode === ActionMode.ADD) {
        this.loaderService.show();
        // this.loading = true;
        this.postsService.addPost(this.newPost).subscribe((post) => {
          this.initPosts(this.messageConfigAdd);
        });
      } else {
        this.loaderService.show();
        // this.loading = true;
        this.postsService.editPost(this.newPost).subscribe((post) => {
          this.initPosts(this.messageConfigEdit);
        });
      }
    }
  }

  showPost(post: Post) {
    this.actionMode = ActionMode.VIEW;
    this.newPost = { ...post };
    this.submitted = false;
    this.postDialog = true;
  }

  deletePost(post: Post) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this post?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.loaderService.show();
        // this.loading = true;
        this.postsService.deletePost(post.id).subscribe(() => {
          this.initPosts(this.messageConfigDelete);
        });
      },
    });
  }
}
