import { Component } from '@angular/core';
import { StatsType } from '@models/stats-type.enum';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent {
  readonly StatsType = StatsType;
  activeIndex: number = 0;
  tabChanged = false;

  onTabChange() {
    this.tabChanged = true;
  }
}
