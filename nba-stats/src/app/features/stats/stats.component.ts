import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Stat } from '@models/stat.interface';
import { StatsType } from '@models/stats-type.enum';
import { forkJoin, of, switchMap } from 'rxjs';
import { LoaderService } from 'src/app/core/services/loader.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { StatsService } from 'src/app/core/services/stats.service';
import { gameFilter } from 'src/app/shared/constants';
import { maxSeasonIndex, removeUndefinedValues } from 'src/app/shared/utils';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
})
export class StatsComponent implements OnInit {
  @Input() type: string = StatsType.OFFENSIVE;
  //$stats2 = this.statsService.getStats();
  stats!: Stat[];

  formGroup!: FormGroup;
  loading = false;

  get isOfensive() {
    return this.type === StatsType.OFFENSIVE;
  }

  //for testin random data

  playerFilter = [
    { name: 'Overall', value: 'Overall' },
    { name: 'Starters', value: 'Starteri' },
    { name: 'C', value: 'C' },
    { name: 'PG', value: 'PG' },
    { name: 'SG', value: 'SG' },
    { name: 'SF', value: 'SF' },
    { name: 'PF', value: 'PF' },
    { name: 'BP', value: 'Best' },
    { name: 'SU', value: 'SpotUp' },
    { name: 'Driver', value: 'Driver' },
    { name: 'CP', value: 'CBS' },
    { name: 'CS', value: 'CSS' },
    { name: 'BHD', value: 'BHD' },
    { name: 'BHS', value: 'BHS' },
    { name: 'MS', value: 'WTF' },
    { name: 'SUS', value: 'SUS' },
  ];

  teamFilter = gameFilter;

  //cap = ['All', 5, 10];
  cap = [
    { name: 'All', value: null },
    { name: '5', value: 5 },
    { name: '10', value: 10 },
  ];

  seasonFilter = [
    { name: 'Regular', value: null },
    { name: 'Before trade', value: 'before_trade' },
    { name: 'After trade', value: 'after_trade' },
    { name: 'Playoff', value: 'playoff' },
  ];
  //seasonFilter = ['All', 'before_trade', 'after_trade', 'playoff'];

  seasonsFilter: string[] = []; //= ['2020_2021', '2021_2022', '2022_2023'];

  constructor(
    protected statsService: StatsService,
    public loaderService: LoaderService,
    protected staticDataService: StaticDataService
  ) {}

  ngOnInit(): void {
    this.initStatsAndSeasons();
  }

  initStatsAndSeasons() {
    this.loaderService.show();
    this.staticDataService
      .getSeasons()
      .pipe(
        switchMap((seasons) => {
          const lastSeason = seasons[maxSeasonIndex(seasons)];
          return forkJoin({
            seasons: of(seasons),
            stats: this.statsService.getStats(
              lastSeason,
              this.type.toLowerCase()
            ),
          });
        })
      )
      .subscribe(({ seasons, stats }) => {
        
        

        this.seasonsFilter = seasons;
        this.stats = stats;

        // this.loading = false;

        this.initFiltersFromGroup();
        this.initFiltersFormListener();
        this.loaderService.hide();
      });
  }

  initFiltersFromGroup() {
    this.formGroup = new FormGroup({
      // selectedCity: new FormControl<City | null>(null),
      playerFilter: new FormControl<any | null>({
        value: null,
        disabled: this.type === StatsType.OFFENSIVE ? true : false,
      }),
      teamFilter: new FormControl<any | null>(null),
      cap: new FormControl<any | null>(this.cap[0]),
      seasonFilter: new FormControl<any | null>(this.seasonFilter[0]),
      season: new FormControl<any | null>(
        this.seasonsFilter[maxSeasonIndex(this.seasonsFilter)]
      ),
    });
  }

  initFiltersFormListener() {
    this.formGroup.valueChanges.subscribe((formValue) => {
      // this.loaderService.show();
      // this.loading = true;
      

      let params = {
        ...formValue,
        seasonFilter: formValue.seasonFilter?.value ?? undefined,
        playerFilter: formValue.playerFilter?.value ?? undefined,
        teamFilter: formValue.teamFilter?.value ?? undefined,
        cap: formValue.cap?.value ?? undefined,
      };
      

      const formattedParams = removeUndefinedValues(params);

      this.loaderService.show();
      this.statsService
        .getStatsWithQueryParams(formattedParams, this.type)
        .subscribe((stats) => {
          this.stats = stats;
          // this.loading = false;
          this.loaderService.hide();
        });
    });
  }
}
