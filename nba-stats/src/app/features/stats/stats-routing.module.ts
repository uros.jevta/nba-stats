import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatsComponent } from './stats.component';
import { ViewComponent } from './view/view.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: StatsComponent,
  // },
  {
    path: '',
    component: ViewComponent,
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatsRoutingModule {}
