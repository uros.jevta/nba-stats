import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsInOutDefensiveComponent } from './stats-in-out-defensive.component';

describe('StatsInOutComponent', () => {
  let component: StatsInOutDefensiveComponent;
  let fixture: ComponentFixture<StatsInOutDefensiveComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StatsInOutDefensiveComponent]
    });
    fixture = TestBed.createComponent(StatsInOutDefensiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
