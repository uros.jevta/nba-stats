import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsInOutContainerComponent } from './stats-in-out-container.component';

describe('StatsInOutContainerComponent', () => {
  let component: StatsInOutContainerComponent;
  let fixture: ComponentFixture<StatsInOutContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StatsInOutContainerComponent]
    });
    fixture = TestBed.createComponent(StatsInOutContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
