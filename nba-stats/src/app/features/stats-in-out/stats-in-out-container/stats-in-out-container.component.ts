import { Component } from '@angular/core';

@Component({
  selector: 'app-stats-in-out-container',
  templateUrl: './stats-in-out-container.component.html',
  styleUrls: ['./stats-in-out-container.component.scss']
})
export class StatsInOutContainerComponent {
  tabChanged = false;

  onTabChange() {
    this.tabChanged = true;
  }
}
