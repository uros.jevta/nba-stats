import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsInOutOffensiveComponent } from './stats-in-out-offensive.component';

describe('StatsInOutOffensiveComponent', () => {
  let component: StatsInOutOffensiveComponent;
  let fixture: ComponentFixture<StatsInOutOffensiveComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StatsInOutOffensiveComponent]
    });
    fixture = TestBed.createComponent(StatsInOutOffensiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
