import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { forkJoin, pairwise, startWith } from 'rxjs';
import { LoaderService } from 'src/app/core/services/loader.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { StatsService } from 'src/app/core/services/stats.service';
import { gameFilter } from 'src/app/shared/constants';
import {
  maxSeasonIndex,
  removeUndefinedValues,
  sortArrayObjectsByDate,
  transformObjectToArray,
} from 'src/app/shared/utils';

@Component({
  selector: 'app-stats-in-out-offensive',
  templateUrl: './stats-in-out-offensive.component.html',
  styleUrls: ['./stats-in-out-offensive.component.scss'],
})
export class StatsInOutOffensiveComponent implements OnInit {
  formGroup!: FormGroup;
  gameFilter = gameFilter;
  teams!: {
    name: string;
    value: string;
  }[];
  seasons!: string[];
  teamPlayers!: {
    name: string;
    value: string;
  }[];
  loading = false;
  statsInOut!: any[];
  statsInOutAllGames!: any[];
  gameDetails!: any;

  constructor(
    private statsService: StatsService,
    private staticDataService: StaticDataService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initSeasonsAndTeams();
  }

  initFiltersFromGroup() {
    this.formGroup = new FormGroup({
      teamId: new FormControl<any | null>(null),
      gameFilter: new FormControl<any | null>(this.gameFilter[0]),
      //includePlayers: new FormControl<any | null>(null),
      excludePlayers: new FormControl<any | null>(null),
      season: new FormControl<any | null>(
        this.seasons[maxSeasonIndex(this.seasons)]
      ),
    });
  }

  initSeasonsAndTeams() {
    this.loaderService.show();

    forkJoin([
      this.staticDataService.getSeasons(),
      this.staticDataService.getTeams(),
    ]).subscribe(([seasons, teams]) => {
      this.seasons = seasons;
      this.teams = transformObjectToArray(teams);

      this.initFiltersFromGroup();
      this.initFiltersFormListener();

      this.loaderService.hide();
    });
  }

  initFiltersFormListener() {
    this.formGroup.valueChanges
      .pipe(startWith(null), pairwise())
      .subscribe(([inital, formValue]) => {
        
        

        if (!formValue?.teamId?.value) {
          return;
        }

        if (inital?.season !== formValue.season) {
          this.getTeamPlayers(formValue);
          return;
        }

        this.loaderService.show();
        // this.loading = true;

        if (inital?.teamId?.value !== formValue.teamId.value) {
          this.getTeamPlayers(formValue);
        } else {
          let excludePlayersIdArray = formValue?.excludePlayers?.map(
            (element: any) => element.value
          );

          let params = {
            teamId: formValue.teamId?.value ?? undefined,
            season: formValue.season ?? undefined,
            excludePlayers:
              excludePlayersIdArray && excludePlayersIdArray.length
                ? excludePlayersIdArray.join(',')
                : null,
            gameFilter: formValue.gameFilter?.value ?? undefined,
          };

          const formattedParams = removeUndefinedValues(params);

          if (params.teamId) {
            this.loaderService.show();

            // this.loading = true;
            this.getOffensive(formattedParams);
          }
        }
      });
  }

  getTeamPlayers(formValue: any) {
    let playersParams = {
      teamId: formValue.teamId.value,
      season: this.formGroup.controls['season'].value,
    };

    this.staticDataService
      .getTeamPlayers(playersParams)
      .subscribe((players) => {
        this.teamPlayers = transformObjectToArray(players);

        this.formGroup.controls['excludePlayers'].setValue(null, {
          emitEvent: false,
        });

        let params = {
          teamId: formValue.teamId?.value ?? undefined,
          season: formValue.season ?? undefined,
          excludePlayers: null,
          gameFilter: formValue.gameFilter?.value ?? undefined,
        };

        const formattedParams = removeUndefinedValues(params);

        if (params.teamId) {
          this.loaderService.show();

          // this.loading = true;
          this.getOffensive(formattedParams);
        } else {
          this.loaderService.hide();
        }
      });
  }

  getOffensive(formattedParams: any) {
    this.loaderService.show();

    this.statsService
      .getStatsInOutOffensive(formattedParams)
      .subscribe((statsInOut) => {
        this.statsInOut = statsInOut;

        let statsInOutAllGamesTemp: any[] = [];

        statsInOut.forEach((item) => {
          let games = [...item.games];

          games.sort(sortArrayObjectsByDate(games));

          games = games.map((game: any, index: number) => {
            return {
              ...game,
              occurrence: games.length,
              first: !index ? true : false,
            };
          });

          statsInOutAllGamesTemp = statsInOutAllGamesTemp.concat(games);
        });

        this.statsInOutAllGames = statsInOutAllGamesTemp.map((value, index) => {
          return {
            ...value,
            index,
          };
        });

        this.loaderService.hide();

        // this.loading = false;
      });
  }

  onDateClick(event: any) {
    this.loaderService.show();

    // this.loading = true;

    const queryParams = {
      date: event.data.date,
      playerId: event.data.playerId,
    };
    this.changeOccurrenceOfPlayer(event.data.player, +1);
    this.statsService.getGameDetails(queryParams).subscribe((gameDetails) => {
      this.gameDetails = gameDetails;

      event.data = this.gameDetails;
      // this.loading = false;
      this.loaderService.hide();
    });
  }

  onRowCollapse(event: any) {
    
    
    this.changeOccurrenceOfPlayer(event.data.player, -1);
  }

  changeOccurrenceOfPlayer(player: string, change: number) {
    // 
    // this.statsInOutAllGames = this.statsInOutAllGames.map((game) => {
    //   return {
    //     ...game,
    //     occurrence:
    //       player === game.player ? game.occurrence + change : game.occurrence,
    //   };
    // });
  }
}
