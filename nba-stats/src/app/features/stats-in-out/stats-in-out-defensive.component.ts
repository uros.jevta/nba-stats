import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { forkJoin, pairwise, startWith } from 'rxjs';
import { LoaderService } from 'src/app/core/services/loader.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { StatsService } from 'src/app/core/services/stats.service';
import { gameFilter } from 'src/app/shared/constants';
import {
  maxSeasonIndex,
  removeUndefinedValues,
  transformObjectToArray,
} from 'src/app/shared/utils';

@Component({
  selector: 'app-stats-in-out-defensive',
  templateUrl: './stats-in-out-defensive.component.html',
  styleUrls: ['./stats-in-out-defensive.component.scss'],
  providers: [MessageService],
})
export class StatsInOutDefensiveComponent implements OnInit {
  formGroup!: FormGroup;
  gameFilter = gameFilter;
  teams!: {
    name: string;
    value: string;
  }[];
  seasons!: string[];
  teamPlayers!: {
    name: string;
    value: string;
  }[];
  loading = false;
  statsInOut!: any[];

  selectionLimitPlayerIn = 2;
  selectionLimitPlayerOut = 2;

  noteShowed = false;

  get includePlayersFromControl() {
    return this.formGroup.controls['includePlayers'];
  }

  get excludePlayersFromControl() {
    return this.formGroup.controls['excludePlayers'];
  }

  constructor(
    private statsService: StatsService,
    private staticDataService: StaticDataService,
    private messageService: MessageService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initSeasonsAndTeams();
  }

  initSeasonsAndTeams() {
    this.loaderService.show();

    forkJoin([
      this.staticDataService.getSeasons(),
      this.staticDataService.getTeams(),
    ]).subscribe(([seasons, teams]) => {
      this.seasons = seasons;
      this.teams = transformObjectToArray(teams);

      this.initFiltersFromGroup();
      this.initFiltersFormListener();

      this.loaderService.hide();
    });
  }

  initFiltersFromGroup() {
    this.formGroup = new FormGroup({
      teamId: new FormControl<any | null>(null),
      gameFilter: new FormControl<any | null>(this.gameFilter[0]),
      includePlayers: new FormControl<any | null>(null),
      excludePlayers: new FormControl<any | null>(null),
      season: new FormControl<any | null>(
        this.seasons[maxSeasonIndex(this.seasons)]
      ),
    });
  }

  initFiltersFormListener() {
    this.includePlayersFromControl.valueChanges.subscribe((includePlayers) => {
      
      if (includePlayers.length === 2) {
        this.selectionLimitPlayerOut = 2;
        this.excludePlayersFromControl.disable({ emitEvent: false });
      } else if (includePlayers.length === 1) {
        this.selectionLimitPlayerOut = 1;
        this.excludePlayersFromControl.enable({ emitEvent: false });
      } else {
        this.excludePlayersFromControl.enable({ emitEvent: false });
        this.selectionLimitPlayerOut = 2;
      }
    });

    this.excludePlayersFromControl.valueChanges.subscribe((excludePlayers) => {
      
      if (excludePlayers.length === 2) {
        this.selectionLimitPlayerIn = 2;
        this.includePlayersFromControl.disable({ emitEvent: false });
      } else if (excludePlayers.length === 1) {
        this.selectionLimitPlayerIn = 1;
        this.includePlayersFromControl.enable({ emitEvent: false });
      } else {
        this.selectionLimitPlayerIn = 2;
        this.includePlayersFromControl.enable({ emitEvent: false });
      }
    });

    this.formGroup.valueChanges
      .pipe(startWith(null), pairwise())
      .subscribe(([inital, formValue]) => {
        
        

        if (!formValue?.teamId?.value) {
          return;
        }

        if (inital?.season !== formValue.season) {
          this.getTeamPlayers(formValue);

          this.selectionLimitPlayerIn = 2;
          this.selectionLimitPlayerOut = 2;
          this.includePlayersFromControl.enable({ emitEvent: false });
          this.excludePlayersFromControl.enable({ emitEvent: false });

          return;
        }

        this.loaderService.show();

        // this.loading = true;

        if (inital?.teamId?.value !== formValue.teamId.value) {
          this.getTeamPlayers(formValue);
        } else {
          let includePlayersIdArray = formValue?.includePlayers?.map(
            (element: any) => element.value
          );

          let excludePlayersIdArray = formValue?.excludePlayers?.map(
            (element: any) => element.value
          );
          

          let params = {
            teamId: formValue.teamId?.value ?? undefined,
            season: formValue.season ?? undefined,
            includePlayers:
              includePlayersIdArray && includePlayersIdArray.length
                ? includePlayersIdArray.join(',')
                : null,
            excludePlayers:
              excludePlayersIdArray && excludePlayersIdArray.length
                ? excludePlayersIdArray.join(',')
                : null,
            gameFilter: formValue.gameFilter?.value ?? undefined,
          };
          
          const formattedParams = removeUndefinedValues(params);
          // const isEveryFilterUsed = !Object.values(params).some(
          //   (el) => el === undefined
          // );
          // 
          if (params.teamId) {
            this.loaderService.show();

            // this.loading = true;
            this.statsService
              .getStatsInOutDefensive(formattedParams)
              .subscribe((statsInOut) => {
                
                this.statsInOut = statsInOut;

                // this.loading = false;
                this.loaderService.hide();
              });
          } else {
            this.loaderService.hide();
          }
        }
      });
  }

  getTeamPlayers(formValue: any) {
    this.loaderService.show();

    let playersParams = {
      teamId: formValue.teamId.value,
      season: this.formGroup.controls['season'].value,
    };

    this.staticDataService
      .getTeamPlayers(playersParams)
      .subscribe((players) => {
        this.teamPlayers = transformObjectToArray(players);
        this.formGroup.controls['includePlayers'].setValue(null, {
          emitEvent: false,
        });
        this.formGroup.controls['excludePlayers'].setValue(null, {
          emitEvent: false,
        });

        let params = {
          teamId: formValue.teamId?.value ?? undefined,
          season: formValue.season ?? undefined,
          includePlayers: null,
          excludePlayers: null,
          gameFilter: formValue.gameFilter?.value ?? undefined,
        };
        
        const formattedParams = removeUndefinedValues(params);
        // const isEveryFilterUsed = !Object.values(params).some(
        //   (el) => el === undefined
        // );
        // 
        if (params.teamId) {
          this.loaderService.show();
          // this.loading = true;
          this.statsService
            .getStatsInOutDefensive(formattedParams)
            .subscribe((statsInOut) => {
              
              this.statsInOut = statsInOut;

              // this.loading = false;
              this.loaderService.hide();
            });
        } else {
          this.loaderService.hide();
        }
      });
  }

  showNote() {
    if (!this.noteShowed && this.teamPlayers?.length) {
      this.messageService.add({
        severity: 'info',
        summary: 'Note',
        detail:
          'You can choose maximum 2 players in "Player IN" and "Player Out" filter. If you are combining In and Out, then only one player can be selected per filter.',
        life: 5000,
      });

      this.noteShowed = true;
    }
  }
}
