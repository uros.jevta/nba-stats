import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatsInOutContainerComponent } from './stats-in-out-container/stats-in-out-container.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: StatsComponent,
  // },
  {
    path: '',
    component: StatsInOutContainerComponent,
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatsInOutRoutingModule {}
