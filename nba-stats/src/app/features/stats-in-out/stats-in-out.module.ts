import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsInOutDefensiveComponent } from './stats-in-out-defensive.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { StatsInOutRoutingModule } from './stats-in-out-routing.module';
import { StatsInOutOffensiveComponent } from './stats-in-out-offensive/stats-in-out-offensive.component';
import { StatsInOutContainerComponent } from './stats-in-out-container/stats-in-out-container.component';
import { CheckboxModule } from 'primeng/checkbox';
import { ToastModule } from 'primeng/toast';
import { GameDetailsComponent } from 'src/app/shared/components/game-details/game-details.component';

@NgModule({
  declarations: [
    StatsInOutDefensiveComponent,
    StatsInOutOffensiveComponent,
    StatsInOutContainerComponent,
  ],
  imports: [
    CommonModule,
    StatsInOutRoutingModule,
    TableModule,
    DropdownModule,
    MultiSelectModule,
    TabViewModule,
    FormsModule,
    ReactiveFormsModule,
    CheckboxModule,
    ToastModule,
    GameDetailsComponent
  ],
})
export class StatsInOutModule {}
