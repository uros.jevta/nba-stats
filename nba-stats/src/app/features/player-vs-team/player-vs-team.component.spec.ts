import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerVsTeamComponent } from './player-vs-team.component';

describe('PlayerVsTeamComponent', () => {
  let component: PlayerVsTeamComponent;
  let fixture: ComponentFixture<PlayerVsTeamComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlayerVsTeamComponent]
    });
    fixture = TestBed.createComponent(PlayerVsTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
