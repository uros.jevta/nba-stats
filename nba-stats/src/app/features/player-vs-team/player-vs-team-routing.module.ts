import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlayerVsTeamComponent } from './player-vs-team.component';

const routes: Routes = [
  {
    path: '',
    component: PlayerVsTeamComponent,
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlayerVsTeamRoutingModule {}
