import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerVsTeamComponent } from './player-vs-team.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { PlayerVsTeamRoutingModule } from './player-vs-team-routing.module';
import { CheckboxModule } from 'primeng/checkbox';
import { GameDetailsComponent } from 'src/app/shared/components/game-details/game-details.component';

@NgModule({
  declarations: [PlayerVsTeamComponent],
  imports: [
    CommonModule,
    PlayerVsTeamRoutingModule,
    TableModule,
    DropdownModule,
    MultiSelectModule,
    TabViewModule,
    FormsModule,
    ReactiveFormsModule,
    CheckboxModule,
    GameDetailsComponent
  ],
})
export class PlayerVsTeamModule {}
