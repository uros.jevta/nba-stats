import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { LoaderService } from 'src/app/core/services/loader.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { StatsService } from 'src/app/core/services/stats.service';
import {
  calculateAverage,
  removeUndefinedValues,
  sortArrayObjectsByDate,
  transformObjectToArray,
} from 'src/app/shared/utils';

@Component({
  selector: 'app-player-vs-team',
  templateUrl: './player-vs-team.component.html',
  styleUrls: ['./player-vs-team.component.scss'],
})
export class PlayerVsTeamComponent implements OnInit {
  teams!: {
    name: string;
    value: string;
  }[];
  players!: {
    name: string;
    value: string;
  }[];
  formGroup!: FormGroup;
  loading = false;
  playerVsTeam!: any;
  playerVsTeamAverage!: any;
  gameDetails!: any;

  playOff = [
    { name: 'Playoff', value: 'true' },
    { name: 'Regular season', value: 'false' },
  ];

  get playerName() {
    return this.formGroup.get('player')?.value.name;
  }

  constructor(
    protected statsService: StatsService,
    private staticDataService: StaticDataService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initPlayersAndTeams();
  }

  initPlayersAndTeams() {
    this.loaderService.show();

    forkJoin([
      this.staticDataService.getPlayers(),
      this.staticDataService.getTeams(),
    ]).subscribe(([players, teams]) => {
      this.players = transformObjectToArray(players);
      this.teams = transformObjectToArray(teams);
      // 
      // 
      this.initFiltersFromGroup();
      this.initFiltersFormListener();

      this.loaderService.hide();
    });
  }

  initFiltersFromGroup() {
    this.formGroup = new FormGroup({
      player: new FormControl<any | null>(null),
      teams: new FormControl<any | null>(null),
      playoff: new FormControl<any | null>(null),
    });
  }

  initFiltersFormListener() {
    this.formGroup.valueChanges.subscribe((formValue) => {
      
      let teamsIdArray = formValue?.teams?.map((element: any) => element.value);

      let params = {
        playerId: formValue.player?.value ?? undefined,
        teamIds: teamsIdArray ? teamsIdArray.join(',') : undefined,
        playoff: formValue.playoff?.value ?? undefined,
      };

      const formattedParams = removeUndefinedValues(params);

      const isEveryFilterUsed = !Object.values(params).some(
        (el) => el === undefined
      );

      if (isEveryFilterUsed) {
        this.loaderService.show();
        // this.loading = true;

        this.statsService
          .getPlayerVsTeam(formattedParams)
          .subscribe((playerVsTeam) => {
            
            this.playerVsTeam = playerVsTeam;

            let objectValues = Object.values(playerVsTeam);

            let averageArray: any[] = [];

            objectValues.forEach((element: any) => {
              let average = calculateAverage(element);
              average.opponent = element[0].opponent;
              // 
              averageArray.push(average);
            });
            

            let modifiedObjectValues = objectValues.map((gamesPerPlayer) => {
              let sortedGamesPerPlayer = [...gamesPerPlayer];
              sortedGamesPerPlayer.sort(
                sortArrayObjectsByDate(sortedGamesPerPlayer)
              );
              return sortedGamesPerPlayer.map((game: any, index: number) => {
                return {
                  ...game,
                  occurrence: gamesPerPlayer.length,
                  first: !index ? true : false,
                };
              });
            });

            

            this.playerVsTeamAverage = averageArray;
            

            

            this.playerVsTeam = modifiedObjectValues
              .flat()
              .map((value, index) => {
                return {
                  ...value,
                  index,
                };
              });
            

            this.loaderService.hide();
            // this.loading = false;
          });
      }
    });
  }

  onDateClick(event: any) {
    this.loaderService.show();
    // this.loading = true;
    
    const queryParams = {
      date: event.data.date,
      playerId: event.data.playerId,
    };
    this.changeOccurrenceOfPlayer(event.data.opponent, +1);
    this.statsService.getGameDetails(queryParams).subscribe((gameDetails) => {
      
      this.gameDetails = gameDetails;

      event.data = this.gameDetails;
      // this.loading = false;
      this.loaderService.hide();
    });
  }

  onRowCollapse(event: any) {
    
    
    this.changeOccurrenceOfPlayer(event.data.opponent, -1);
  }

  changeOccurrenceOfPlayer(opponent: string, change: number) {
    
    this.playerVsTeam = this.playerVsTeam.map((game: any) => {
      return {
        ...game,
        occurrence:
        opponent === game.opponent ? game.occurrence + change : game.occurrence,
      };
    });
  }
}
