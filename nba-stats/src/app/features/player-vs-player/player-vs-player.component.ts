import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { LoaderService } from 'src/app/core/services/loader.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { StatsService } from 'src/app/core/services/stats.service';
import {
  calculateAverage,
  removeUndefinedValues,
  sortArrayObjectsByDate,
  transformObjectToArray,
} from 'src/app/shared/utils';

@Component({
  selector: 'app-player-vs-player',
  templateUrl: './player-vs-player.component.html',
  styleUrls: ['./player-vs-player.component.scss'],
})
export class PlayerVsPlayerComponent implements OnInit {
  formGroup!: FormGroup;
  players!: {
    name: string;
    value: string;
  }[];
  playerVsPlayer!: any;
  playerVsPlayerAverage!: any;
  gameDetails!: any;

  loading = false;

  homeAway = [
    { name: 'Overall', value: null },
    { name: 'Home', value: 'home' },
    { name: 'Away', value: 'away' },
  ];

  playOff = [
    { name: 'Playoff', value: 'true' },
    { name: 'Regular season', value: 'false' },
  ];

  groupedRows = ['player, test'];

  get playerName() {
    return this.formGroup.get('player')?.value.name;
  }

  constructor(
    protected statsService: StatsService,
    private staticDataService: StaticDataService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.initPlayers();
  }

  initPlayers() {
    this.loaderService.show();
    this.staticDataService.getPlayers().subscribe((players) => {
      this.players = transformObjectToArray(players);

      this.initFiltersFromGroup();
      this.initFiltersFormListener();

      this.loaderService.hide();
    });
  }

  initFiltersFromGroup() {
    this.formGroup = new FormGroup({
      player: new FormControl<any | null>(null),
      opponents: new FormControl<any | null>(null),
      homeAway: new FormControl<any | null>(this.homeAway[0]),
      playoff: new FormControl<any | null>(null),
    });
  }

  initFiltersFormListener() {
    this.formGroup.valueChanges.subscribe((formValue) => {
      let opponentsIdArray = formValue?.opponents?.map(
        (element: any) => element.value
      );

      let params = {
        playerId: formValue.player?.value ?? undefined,
        opponentIds: opponentsIdArray ? opponentsIdArray.join(',') : undefined,
        homeAway: formValue.homeAway?.value,
        playoff: formValue.playoff?.value ?? undefined,
      };
      //

      const formattedParams = removeUndefinedValues(params);

      const isEveryFilterUsed = !Object.values(params).some(
        (el) => el === undefined
      );
      //

      if (isEveryFilterUsed) {
        this.loaderService.show();
        // this.loading = true;

        this.statsService
          .getPlayerVsPlayer(formattedParams)
          .subscribe((playerVsPlayer) => {
            // this.playerVsPlayer = playerVsPlayer;

            let objectValues = Object.values(playerVsPlayer);

            let averageArray: any[] = [];

            objectValues.forEach((element: any) => {
              let average = calculateAverage(element);
              average.player = element[0].player;

              averageArray.push(average);
            });

            let modifiedObjectValues = objectValues.map((gamesPerPlayer) => {
              let sortedGamesPerPlayer = [...gamesPerPlayer];
              sortedGamesPerPlayer.sort(
                sortArrayObjectsByDate(sortedGamesPerPlayer)
              );
              return sortedGamesPerPlayer.map((game: any, index: number) => {
                return {
                  ...game,
                  occurrence: gamesPerPlayer.length,
                  first: !index ? true : false,
                };
              });
            });

            this.playerVsPlayerAverage = averageArray;

            this.playerVsPlayer = modifiedObjectValues
              .flat()
              .map((value, index) => {
                return {
                  ...value,
                  index,
                };
              });
            // Object.values(objectValues)
            //

            this.loaderService.hide();
            // this.loading = false;
          });
      }
    });
  }

  onDateClick(event: any) {
    this.loaderService.show();
    // this.loading = true;

    const queryParams = {
      date: event.data.date,
      playerId: event.data.playerId,
    };
    this.changeOccurrenceOfPlayer(event.data.player, +1);
    this.statsService.getGameDetails(queryParams).subscribe((gameDetails) => {
      this.gameDetails = gameDetails;

      event.data = this.gameDetails;
      this.loaderService.hide();
      this.loading = false;
    });
  }

  onRowCollapse(event: any) {
    this.changeOccurrenceOfPlayer(event.data.player, -1);
  }

  changeOccurrenceOfPlayer(player: string, change: number) {
    this.playerVsPlayer = this.playerVsPlayer.map((game: any) => {
      return {
        ...game,
        occurrence:
          player === game.player ? game.occurrence + change : game.occurrence,
      };
    });
  }
}
