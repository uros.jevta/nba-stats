import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerVsPlayerRoutingModule } from './player-vs-player-routing.module';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabViewModule } from 'primeng/tabview';
import { PlayerVsPlayerComponent } from './player-vs-player.component';
import { ButtonModule } from 'primeng/button';
import { GameDetailsComponent } from 'src/app/shared/components/game-details/game-details.component';

@NgModule({
  declarations: [PlayerVsPlayerComponent],
  imports: [
    CommonModule,
    PlayerVsPlayerRoutingModule,
    TableModule,
    DropdownModule,
    MultiSelectModule,
    TabViewModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    GameDetailsComponent
  ],
})
export class PlayerVsPlayerModule {}
