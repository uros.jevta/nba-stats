import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';

import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    
    const isAuth = this.authService.isAuthenticated();
    let roles = this.authService.getRoles();
    // 
    // let test1 = route.data['role'];
    // 
    // let test2 = !roles?.some((r) => route.data['role'].includes(r));
    // 

    if(!roles) {
      this.authService.loginAsGuest();
    }

    if (
      route.data['role'] &&
      !roles?.some((r) => route.data['role'].includes(r))
    ) {
      this.router.navigate(['/']);
      return false;
    }

    return true;
  }
}
