import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';

import { environment } from '@env/environment';
import { catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { MessageService } from 'primeng/api';
import { ApiPath } from 'src/app/core/constants/api-path';
import { LoaderService } from 'src/app/core/services/loader.service';

@Injectable({
  providedIn: 'root',
})
export class TokenInterceptor implements HttpInterceptor {
  private readonly apiURL: string = environment.apiUrl;

  constructor(
    public _authService: AuthService,
    private messageService: MessageService,
    public loaderService: LoaderService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next
      .handle(this.performRequest(request))
      .pipe(
        catchError((error: HttpErrorResponse) =>
          this.processRequestError(error, request, next)
        )
      );
  }

  private performRequest(request: HttpRequest<any>): HttpRequest<any> {
    if (this._authService.isAuthenticated() && request.url.match(this.apiURL)) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${sessionStorage.getItem('token')}`,
        },
      });
      return request;
    } else return request.clone();
  }

  private processRequestError(
    error: HttpErrorResponse,
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const url = request.url.replace(environment.apiUrl + '/', '');

    if ([401, 403].includes(error.status)) {
      if (url === ApiPath.LOGIN) {
        this.messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: 'Invalid username or password',
          life: 3000,
        });
      } else if (url === ApiPath.REGISTER) {
        this.messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: 'Failed to register',
          life: 3000,
        });
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: 'Access denied',
          life: 3000,
        });
        this._authService.logout();
      }
      this.loaderService.hide();
    }

    if ([500].includes(error.status)) {
      this.messageService.add({
        severity: 'error',
        summary: 'Error',
        detail: 'Server error',
        life: 3000,
      });
      this.loaderService.hide();
    }

    return throwError(error);
  }
}
