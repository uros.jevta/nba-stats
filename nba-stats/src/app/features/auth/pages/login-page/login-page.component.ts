import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiPath } from 'src/app/core/constants/api-path';
import { UserService } from 'src/app/core/services/user.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';
import { LoaderService } from 'src/app/core/services/loader.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  providers: [MessageService],
})
export class LoginPageComponent {
  public loginPageForm!: FormGroup;
  public registerPageForm!: FormGroup;
  loginSignupToggle = false;

  isLoading$ = this.loaderService.isLoading.asObservable();

  messageConfigAdd = {
    severity: 'success',
    summary: 'Successful',
    detail: 'User Created',
    life: 3000,
  };

  constructor(
    private readonly _authService: AuthService,
    private readonly _formBuilder: FormBuilder,
    private readonly _router: Router,
    private userService: UserService,
    private staticDataService: StaticDataService,
    public loaderService: LoaderService,
    private messageService: MessageService
  ) {
    this._initLoginForm();
    this._initRegisterForm();
  }
  private _initLoginForm(): void {
    this.loginPageForm = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  private _initRegisterForm(): void {
    this.registerPageForm = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  public onLogin(): void {
    const { username, password } = this.loginPageForm.value;

    this.loginPageForm.markAsTouched();

    if (this.loginPageForm.invalid) return;

    this.loaderService.show();
    this._authService.login(username, password).subscribe(() => {
      // this._router.navigate(['/', ApiPath.ADMIN]);
      this.staticDataService.resetCache();
      this._router.navigate(['/']);
      this.loaderService.hide();
    });
  }

  register() {
    const { username, password } = this.registerPageForm.value;

    const newUser = {
      username,
      password,
    };

    this.loaderService.show();
    this.userService.addUser(newUser).subscribe(() => {
      this.loaderService.hide();
      this.messageService.add(this.messageConfigAdd);
      this.setLoginSignupToggleFalse();
      // this._router.navigate(['/', ApiPath.ADMIN]);
      // this._router.navigate(['/']);
    });
  }

  setLoginSignupToggleTrue() {
    this.loginSignupToggle = true;
  }

  setLoginSignupToggleFalse() {
    this.loginSignupToggle = false;
  }

  enterAsGuest() {
    this.staticDataService.resetCache();
    this._authService.loginAsGuest();
    this._router.navigate(['/']);
  }
}
