import { UserRoleEnum } from '../enums/user-role.enum';

export interface JwtResponse {
  status: string;
  message: string;
  username: string;
  token: string;
  expires: number;
  roles: UserRoleEnum[];
}
