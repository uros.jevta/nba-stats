import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { LayoutComponent } from './layout/layout.component';
import { RoleRouteDirective } from 'src/app/shared/directives/role-route.directive';



@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ProgressSpinnerModule,
    RoleRouteDirective
  ]
})
export class DashboardModule { }
