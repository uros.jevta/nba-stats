import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AuthGuard } from 'src/app/features/auth/guards/auth.guard';
import { UserRoleEnum } from '../../enums/user-role.enum';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../../../features/stats/stats.module').then(
            (m) => m.StatsModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'pvp',
        loadChildren: () =>
          import(
            '../../../features/player-vs-player/player-vs-player.module'
          ).then((m) => m.PlayerVsPlayerModule),
        canActivate: [AuthGuard],
      },
      {
        path: 'stats-in-out',
        loadChildren: () =>
          import('../../../features/stats-in-out/stats-in-out.module').then(
            (m) => m.StatsInOutModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'log',
        loadChildren: () =>
          import('../../../features/game-log/game-log.module').then(
            (m) => m.GameLogModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'pvt',
        loadChildren: () =>
          import('../../../features/player-vs-team/player-vs-team.module').then(
            (m) => m.PlayerVsTeamModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'admin',
        loadChildren: () =>
          import('../../../features/admin/admin.module').then(
            (module) => module.AdminModule
          ),
        canActivate: [AuthGuard],
        data: {
          role: [UserRoleEnum.ROLE_SUPER_ADMIN],
          defaultReuseStrategy: true,
        },
      },
      {
        path: 'posts',
        loadChildren: () =>
          import('../../../features/posts/posts.module').then(
            (module) => module.PostsModule
          ),
        canActivate: [AuthGuard],
        data: {
          role: [UserRoleEnum.ROLE_FULL_ACCESS, UserRoleEnum.ROLE_SUPER_ADMIN],
          defaultReuseStrategy: true,
        },
      },
      {
        path: 'tips',
        loadChildren: () =>
          import('../../../features/tips/tips.module').then(
            (module) => module.TipsModule
          ),
        canActivate: [AuthGuard],
        data: {
          role: [
            UserRoleEnum.ROLE_FULL_ACCESS,
            UserRoleEnum.ROLE_TIPS_ACCESS,
            UserRoleEnum.ROLE_TIPS_AND_STATISTICS_ACCESS,
            UserRoleEnum.ROLE_SUPER_ADMIN,
          ],
          defaultReuseStrategy: true,
        },
      },
      {
        path: 'contact',
        loadComponent: () =>
          import('../../../features/contact/contact.component').then(
            (module) => module.ContactComponent
          ),
      },
      {
        path: 'categories',
        loadComponent: () =>
          import('../../../features/player-positions/player-positions.component').then(
            (module) => module.PlayerPositionsComponent
          ),
        data: {
          role: [UserRoleEnum.ROLE_FULL_ACCESS],
          defaultReuseStrategy: true,
        },
        canActivate: [AuthGuard],
      },
      {
        path: '**',
        loadChildren: () =>
          import('../../../features/stats/stats.module').then(
            (m) => m.StatsModule
          ),
        canActivate: [AuthGuard],
      },
      // {
      //   path: '**',
      //   loadChildren: () =>
      //     import('../../../features/stats/stats.module').then(
      //       (m) => m.StatsModule
      //     ),
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
