import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { UserRoleEnum } from 'src/app/core/enums/user-role.enum';
import { AuthService } from 'src/app/core/services/auth.service';
import { LoaderService } from 'src/app/core/services/loader.service';
import { StaticDataService } from 'src/app/core/services/static-data.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit, AfterViewChecked {
  navigationOptions = [
    {
      label: 'Base stats',
      value: '',
      roles: [UserRoleEnum.ROLE_GUEST_ACCESS],
    },
    {
      label: 'In/out players',
      value: 'stats-in-out',
      roles: [UserRoleEnum.ROLE_GUEST_ACCESS],
    },
    {
      label: 'Player vs team',
      value: 'pvt',
      roles: [UserRoleEnum.ROLE_GUEST_ACCESS],
    },
    {
      label: 'Player vs player',
      value: 'pvp',
      roles: [UserRoleEnum.ROLE_GUEST_ACCESS],
    },
    {
      label: 'Players stats',
      value: 'log',
      roles: [UserRoleEnum.ROLE_GUEST_ACCESS],
    },
    {
      label: 'Posts',
      value: 'posts',
      roles: [UserRoleEnum.ROLE_FULL_ACCESS],
    },
    {
      label: 'Tips',
      value: 'tips',
      roles: [
        UserRoleEnum.ROLE_FULL_ACCESS,
        UserRoleEnum.ROLE_TIPS_ACCESS,
        UserRoleEnum.ROLE_TIPS_AND_STATISTICS_ACCESS,
        UserRoleEnum.ROLE_SUPER_ADMIN,
      ],
    },
    {
      label: 'Admin',
      value: 'admin',
      roles: [UserRoleEnum.ROLE_SUPER_ADMIN],
    },
    {
      label: 'Category',
      value: 'categories',
      roles: [UserRoleEnum.ROLE_FULL_ACCESS],
    },
    {
      label: 'Contact Us',
      value: 'contact',
      roles: [UserRoleEnum.ROLE_GUEST_ACCESS],
    },
  ];
  username!: string | null;
  isLoading$ = this.loaderService.isLoading.asObservable();
  userRoles!: string;
  humburgerMenuState = false;

  isGuest = this.authService.isGuest();

  constructor(
    public authService: AuthService,
    public loaderService: LoaderService,
    private staticDataService: StaticDataService,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.username = this.authService.userName();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  toggleHumburgerMenu() {
    this.humburgerMenuState = !this.humburgerMenuState;
  }

  logout() {
    this.staticDataService.resetCache();
    this.authService.logout();
  }
}
