import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';
import { StaticDataService } from '../services/static-data.service';

@Injectable({
  providedIn: 'root',
})
export class ApiCacheInterceptor implements HttpInterceptor {
  private readonly apiURL: string = environment.apiUrl;

  // private cache = new Map<string, HttpResponse<any>>();

  private endpointsToCache = new Set([
    '/data/seasons',
    '/data/roles',
    '/data/players',
    '/data/teams',
  ]);

  constructor(private staticDataService: StaticDataService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const url = request.url.replace(environment.apiUrl, '');

    if (this.endpointsToCache.has(url)) {
      const cachedResponse = this.staticDataService.getCache().get(request.url)
      //const cachedResponse = this.cache.get(request.url);

      if (cachedResponse) {
        return of(cachedResponse);
      }

      return next.handle(request).pipe(
        tap((response) => {
          if (response instanceof HttpResponse) {
            this.staticDataService.setCache(request.url, response);
            // this.cache.set(request.url, response);
          }
        })
      );
    }

    return next.handle(request);
  }
}
