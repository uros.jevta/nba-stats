export class ApiPath {
  public static LOGIN = 'login';
  public static AUTH = 'auth';
  public static ADMIN = 'admin';
  public static REGISTER = 'register';
}
