import {
  ActivatedRouteSnapshot,
  DetachedRouteHandle,
  Route,
  RouteReuseStrategy,
} from '@angular/router';

export class CustomReuseStrategy implements RouteReuseStrategy {
  private handlers: Map<Route, DetachedRouteHandle> = new Map();

  constructor() {}

  public shouldDetach(route: ActivatedRouteSnapshot): boolean {
    // return true;
    return !this.shouldIgnoreReuseStrategy(route);
  }

  public store(
    route: ActivatedRouteSnapshot,
    handle: DetachedRouteHandle
  ): void {
    if (!route.routeConfig) return;
    this.handlers.set(route.routeConfig, handle);
  }

  public shouldAttach(route: ActivatedRouteSnapshot): boolean {
    if (this.shouldResetReuseStrategy(route)) {
      this.deactivateAllHandles();
      return false;
    }

    if (this.shouldIgnoreReuseStrategy(route)) {
      return false;
    }

    return !!route.routeConfig && !!this.handlers.get(route.routeConfig);
  }

  public retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle | null {
    if (!route.routeConfig || !this.handlers.has(route.routeConfig))
      return null;
    return this.handlers.get(route.routeConfig)!;
  }

  public shouldReuseRoute(
    future: ActivatedRouteSnapshot,
    curr: ActivatedRouteSnapshot
  ): boolean {
    return future.routeConfig === curr.routeConfig;
  }

  private shouldIgnoreReuseStrategy(route: ActivatedRouteSnapshot): boolean {
    return route.data && route.data['defaultReuseStrategy'];
  }

  private shouldResetReuseStrategy(route: ActivatedRouteSnapshot): boolean {
    let snapshot: ActivatedRouteSnapshot = route;

    // while (snapshot.children && snapshot.children.length) {
    //     snapshot = snapshot.children[0];
    // }

    return snapshot.data && snapshot.data['resetReuseStrategy'];
  }

  private deactivateAllHandles(): void {
    // this.handles.forEach((handle: DetachedRouteHandle) => this.destroyComponent(handle));
    this.handlers.clear();
  }
}
