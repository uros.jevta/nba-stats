/// <reference lib="webworker" />

onmessage = ({ data }) => {
  setTimeout((): void => {
    postMessage(true);
  }, data);
};
