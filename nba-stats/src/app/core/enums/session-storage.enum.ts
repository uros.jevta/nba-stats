export enum SessionStorage {
  USER_NAME = 'userName',
  USER_ROLES = 'roles',
  TOKEN = 'token',
  EXPIRATION = 'expiration',
}
