import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StaticDataService {
  private cache = new Map<string, HttpResponse<any>>();

  constructor(private readonly http: HttpClient) {}

  getRoles(): Observable<string[]> {
    return this.http.get<string[]>(`${environment.apiUrl}/data/roles`);
  }

  getTeamPlayers(query: any): Observable<string[]> {
    let params = new HttpParams({ fromObject: query });
    return this.http.get<string[]>(`${environment.apiUrl}/data/team-players`, {
      params: params
    });
  }

  getSeasons(): Observable<string[]> {
    return this.http.get<string[]>(`${environment.apiUrl}/data/seasons`);
  }

  getPlayers(): Observable<string[]> {
    return this.http.get<string[]>(`${environment.apiUrl}/data/players`);
  }

  getTeams(): Observable<string[]> {
    return this.http.get<string[]>(`${environment.apiUrl}/data/teams`);
  }

  resetCache() {
    this.cache.clear()
  }

  setCache(url: string, response: HttpResponse<any>) {
    this.cache.set(url, response)
  }

  getCache() {
    return this.cache;
  }
  
}
