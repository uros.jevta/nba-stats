import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Stat } from '@models/stat.interface';
import { environment } from '@env/environment';
import { GameLog } from '@models/game-log.interface';

@Injectable({
  providedIn: 'root',
})
export class StatsService {
  constructor(private http: HttpClient) {}

  getStats(season: string, type: string): Observable<Stat[]> {
    return this.http.get<Stat[]>(
      `${environment.apiUrl}/team-stats/${type}?season=${season}`
    );
  }

  getStatsWithQueryParams(query: any, type: string) {
    const params = new HttpParams({ fromObject: query });
    return this.http.get<Stat[]>(`${environment.apiUrl}/team-stats/${type}`, {
      params: params,
    });
  }

  getPlayerVsPlayer(query: any) {
    let params = new HttpParams({ fromObject: query });
    return this.http.get<any[]>(`${environment.apiUrl}/player-vs-player`, {
      params: params,
    });
  }

  getGameLogs(query: any) {
    const params = new HttpParams({ fromObject: query });
    return this.http.get<GameLog[]>(`${environment.apiUrl}/game-log`, {
      params: params,
    });
  }

  getGameDetails(query: any) {
    const params = new HttpParams({ fromObject: query });
    return this.http.get<GameLog[]>(
      `${environment.apiUrl}/player-vs-player/game-details`,
      {
        params: params,
      }
    );
  }

  getPlayerVsTeam(query: any) {
    let params = new HttpParams({ fromObject: query });
    return this.http.get<any[]>(`${environment.apiUrl}/player-vs-team`, {
      params: params,
    });
  }

  getStatsInOutDefensive(query: any) {
    let params = new HttpParams({ fromObject: query });
    return this.http.get<GameLog[]>(`${environment.apiUrl}/team-with-without/defensive`, {
      params: params,
    });
  }

  getStatsInOutOffensive(query: any) {
    let params = new HttpParams({ fromObject: query });
    return this.http.get<GameLog[]>(`${environment.apiUrl}/team-with-without/offensive`, {
      params: params,
    });
  }

  getPlayerSpecialPositions(query: any) {
    let params = new HttpParams({ fromObject: query });
    return this.http.get<GameLog[]>(`${environment.apiUrl}/player-special-positions`, {
      params: params,
    });
  }
}
