import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, first, tap } from 'rxjs/operators';
import { ApiPath } from '../constants/api-path';
import { SessionStorage } from '../enums/session-storage.enum';
import { JwtResponse } from '../models/jwt-response.model';
import { UserRoleEnum } from '../enums/user-role.enum';
// import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _apiUrl = environment.apiUrl;
  private _timeoutWorker: Worker | null = null;
  private _isAuthenticated = false;

  messageConfigAdd = {
    severity: 'success',
    summary: 'Successful',
    detail: 'Post Created',
    life: 3000,
  };

  constructor(
    private readonly _http: HttpClient,
    private readonly _router: Router // private messageService: MessageService
  ) {}

  public login(username: string, password: string): Observable<JwtResponse> {
    const headers = new HttpHeaders();
    // .append('Content-Type', 'application/json')
    // .append('Access-Control-Allow-Headers', 'Content-Type')
    // .append(
    //   'Access-Control-Allow-Origin',
    //   'strict-origin-when-cross-origin'
    // );

    return this._http
      .post<JwtResponse>(
        `${this._apiUrl}/login`,
        {
          username,
          password,
        },
        { headers: headers }
      )
      .pipe(
        tap((response: JwtResponse) => {
          
          this.saveAuthData(response);
          this.autoAuthUser();
        }),
        catchError((error: HttpErrorResponse) => {
          
          // this.messageService.add(this.messageConfigAdd);
          return throwError(() => error);
        })
      );
  }

  public saveAuthData(jwtResponse: JwtResponse): void {
    sessionStorage.setItem(SessionStorage.TOKEN, jwtResponse.token);
    sessionStorage.setItem(SessionStorage.USER_NAME, jwtResponse.username);
    sessionStorage.setItem(
      SessionStorage.USER_ROLES,
      JSON.stringify(jwtResponse.roles)
    );
    // sessionStorage.setItem(
    //   SessionStorage.EXPIRATION,
    //   new Date(jwtResponse.expires).toISOString()
    // );
  }

  public autoAuthUser(): void {
    this._isAuthenticated = true;

    // const token = sessionStorage.getItem(SessionStorage.TOKEN);
    // const expirationDate = sessionStorage.getItem(SessionStorage.EXPIRATION);

    // if (!token || !expirationDate) return;

    // const now = new Date();
    // const expiresIn = new Date(expirationDate).getTime() - now.getTime();

    // if (expiresIn > 0) {
    //   this.initAuthTimer();
    //   this.setAuthTimer(expiresIn);
    //   this._isAuthenticated = true;
    // }
  }

  private initAuthTimer(): void {
    if (!this._timeoutWorker) {
      this._timeoutWorker = new Worker(
        new URL('../helper/timeout.worker', import.meta.url)
      );
    }
  }

  private setAuthTimer(duration: number): void {
    if (Worker && this._timeoutWorker) {
      const expirationGap = 20000;

      this._timeoutWorker.postMessage(duration - expirationGap);
      this._timeoutWorker.onmessage = () => this.tryRenewToken();
    }
  }

  public tryRenewToken(): void {
    this._http
      .get<JwtResponse>(`${this._apiUrl}/users/renew`)
      .pipe(first())
      .subscribe((data) => {
        this.saveAuthData(data);
        this.autoAuthUser();
      });
  }
  public logout(): void {
    this._isAuthenticated = false;
    this.clearAuthTimer();
    this.clearAuthData();
    this._router.navigate(['/', ApiPath.LOGIN]);
  }

  public isAuthenticated(): boolean {
    return (
      this._isAuthenticated || !!sessionStorage.getItem(SessionStorage.TOKEN)
    );
  }

  public userName(): string | null {
    return sessionStorage.getItem(SessionStorage.USER_NAME);
  }

  public getRoles(): string[] | null {
    if (sessionStorage.getItem(SessionStorage.USER_ROLES)) {
      return JSON.parse(
        sessionStorage.getItem(SessionStorage.USER_ROLES) as string
      );
    }
    return null;
  }

  private clearAuthTimer(): void {
    if (this._timeoutWorker) {
      this._timeoutWorker.terminate();
      this._timeoutWorker = null;
    }
  }

  private clearAuthData(): void {
    sessionStorage.removeItem(SessionStorage.TOKEN);
    sessionStorage.removeItem(SessionStorage.USER_NAME);
    sessionStorage.removeItem(SessionStorage.EXPIRATION);
    sessionStorage.removeItem(SessionStorage.USER_ROLES);
  }

  public loginAsGuest(): void {
    sessionStorage.setItem(
      SessionStorage.USER_ROLES,
      JSON.stringify([UserRoleEnum.ROLE_GUEST_ACCESS])
    );
    sessionStorage.setItem(SessionStorage.USER_NAME, 'Guest');
    // this.autoAuthUser();
  }

  public isGuest(): boolean {
    return sessionStorage.getItem(SessionStorage.USER_NAME) === 'Guest';
  }

  public isAdmin(): boolean {
    return !!this.getRoles()?.includes(UserRoleEnum.ROLE_SUPER_ADMIN);
  }
}
