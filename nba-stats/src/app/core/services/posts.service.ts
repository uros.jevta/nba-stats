import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Post } from '@models/post.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  constructor(private readonly http: HttpClient) {}

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${environment.apiUrl}/posts`);
  }

  addPost(post: Post) {
    return this.http.post<Post>(`${environment.apiUrl}/posts`, post);
  }

  editPost(post: Post): Observable<Post> {
    return this.http.put<Post>(`${environment.apiUrl}/posts`, post);
  }

  deletePost(id?: string): Observable<Post> {
    return this.http.delete<Post>(`${environment.apiUrl}/posts/${id}`);
  }
}
