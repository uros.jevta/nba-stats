import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { User } from '@models/user.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private readonly http: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/users`);
  }

  addUser(user: User) {
    return this.http.post<User>(`${environment.apiUrl}/register`, user);
  }

  editUser(user: User): Observable<User> {
    return this.http.put<User>(`${environment.apiUrl}/users`, user);
  }

  deleteUser(id?: string): Observable<null> {
    return this.http.delete<null>(`${environment.apiUrl}/users/${id}`);
  }
}
