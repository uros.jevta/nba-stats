import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Tip } from '@models/tip.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TipsService {
  constructor(private readonly http: HttpClient) {}

  getTips(): Observable<Tip[]> {
    return this.http.get<Tip[]>(`${environment.apiUrl}/tips`);
  }

  addTip(tip: Tip) {
    return this.http.post<Tip>(`${environment.apiUrl}/tips`, tip);
  }

  editTip(tip: Tip): Observable<Tip> {
    return this.http.put<Tip>(`${environment.apiUrl}/tips`, tip);
  }

  deleteTip(id?: string): Observable<Tip> {
    return this.http.delete<Tip>(`${environment.apiUrl}/tips/${id}`);
  }
}
