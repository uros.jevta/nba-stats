import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { PlayerPosition } from '@models/player-position.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PlayerPositionService {
  constructor(private readonly http: HttpClient) {}

  getPlayerPositions(teamId: string): Observable<PlayerPosition[]> {
    return this.http.get<PlayerPosition[]>(`${environment.apiUrl}/player-positions/${teamId}`);
  }

  addPlayerPosition(playerPosition: PlayerPosition) {
    return this.http.post<PlayerPosition>(`${environment.apiUrl}/player-positions`, playerPosition);
  }

  editPlayerPosition(playerPositions: PlayerPosition[]): Observable<PlayerPosition> {
    return this.http.put<PlayerPosition>(`${environment.apiUrl}/player-positions`, playerPositions);
  }

  deletePlayerPosition(id?: string): Observable<null> {
    return this.http.delete<null>(`${environment.apiUrl}/player-positions/${id}`);
  }
}
