import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Game } from '@models/game.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(private readonly http: HttpClient) {}

  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(`${environment.apiUrl}/game`);
  }

  addGame(game: Game) {
    return this.http.post<Game>(`${environment.apiUrl}/game`, game);
  }

  editGame(game: Game): Observable<Game> {
    return this.http.put<Game>(`${environment.apiUrl}/game/${game.id}`, game.position);
  }

  deleteGame(id?: string): Observable<null> {
    return this.http.delete<null>(`${environment.apiUrl}/game/${id}`);
  }
}
