import { Component } from '@angular/core';
import { MessageService } from 'primeng/api';
import { LoaderService } from './core/services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'nba-stats';

  // constructor(public loaderService: LoaderService) {}
}
